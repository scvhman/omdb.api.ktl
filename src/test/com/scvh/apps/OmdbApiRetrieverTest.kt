import org.junit.Assert
import org.junit.Test
import java.util.*

class OmdbApiRetrieverTest {

    @Test
    fun testOmdbApiById() {
        val omdb = OmdbAPIRetriever()
        print("Enter valid api key")
        val apiKey = Scanner(System.`in`).nextLine()
        val omdbMovie = omdb.retrieveMovie("tt0080120", apiKey)
        Assert.assertEquals("The Warriors", omdbMovie?.title)
    }

    @Test
    fun testOmdbByTitle() {
        val omdb = OmdbAPIRetriever()
        print("Enter valid api key")
        val apiKey = Scanner(System.`in`).nextLine()
        val omdbMovie = omdb.retrieveMovie("The Warriors", 1979, apiKey)
        Assert.assertEquals("The Warriors", omdbMovie?.title)
    }
}