import main.apps.Movie
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OmdbAPIRetriever {

    fun retrieveMovie(id: String, token: String): Movie? {
        return getRetrofitApi().getMovie(id, token).execute().body()
    }

    fun retrieveMovie(name: String, year: Int, token: String): Movie? {
        return getRetrofitApi().getMovie(name, year, token).execute().body()
    }

    private fun getRetrofitApi(): RetrofitOmdb {
        return Retrofit.Builder().baseUrl("https://www.omdbapi.com")
                .addConverterFactory(GsonConverterFactory.create()).build().create(RetrofitOmdb::class.java)
    }

}